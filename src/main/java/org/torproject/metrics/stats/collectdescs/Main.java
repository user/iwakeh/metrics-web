/* Copyright 2015--2020 The Tor Project
 * See LICENSE for licensing information */

package org.torproject.metrics.stats.collectdescs;

import org.torproject.descriptor.DescriptorCollector;
import org.torproject.descriptor.DescriptorSourceFactory;

import java.io.File;

public class Main {

  private static final File baseDir = new File(
      org.torproject.metrics.stats.main.Main.modulesDir, "collectdescs");

  /** Executes this data-processing module. */
  public static void main(String[] args) {

    /** Fetch recent remote files from a CollecTor instance that do not yet
     *  exist locally and possibly delete local files that do not exist remotely
     *  anymore.
     *
     * <pre>void collectDescriptors(String collecTorBaseUrl,
     *     String[] remoteDirectories, long minLastModified,
     *     File localDirectory, boolean deleteExtraneousLocalFiles);
     * </pre>
     *
     * @param collecTorBaseUrl CollecTor base URL without trailing slash,
     *     e.g., {@code "https://collector.torproject.org"}
     *
     * @param remoteDirectories Remote directories to collect descriptors
     *     from, e.g.,
     *     {@code "recent/relay-descriptors/server-descriptors"}, without
     *     processing subdirectories unless they are explicitly listed.
     *     Leading and trailing slashes will be ignored, i.e.,
     *     {@code "/abc/xyz/"} results in the same downloads as
     *     {@code "abc/xyz"}.
     *
     * @param minLastModified Minimum last-modified time in milliseconds of
     *     files to be collected, or 0 for collecting all files
     *
     * @param localDirectory Directory where collected files will be written
     *
     * @param deleteExtraneousLocalFiles Whether to delete all local files
     *     that do not exist remotely anymore
     *
     */
    DescriptorCollector collector =
        DescriptorSourceFactory.createDescriptorCollector();
    collector.collectDescriptors(
        "https://collector.torproject.org", new String[] {
            "/recent/bridgedb-metrics/",
            "/recent/bridge-descriptors/extra-infos/",
            "/recent/bridge-descriptors/server-descriptors/",
            "/recent/bridge-descriptors/statuses/",
            "/recent/exit-lists/",
            "/recent/relay-descriptors/consensuses/",
            "/recent/relay-descriptors/extra-infos/",
            "/recent/relay-descriptors/server-descriptors/",
            "/recent/relay-descriptors/votes/",
            "/recent/onionperf/",
            "/recent/webstats/"
        }, 0L, org.torproject.metrics.stats.main.Main.descriptorsDir, true);
  }
}
